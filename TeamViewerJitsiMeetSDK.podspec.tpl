Pod::Spec.new do |s|
  s.name             = 'TeamViewerJitsiMeetSDK'
  s.version          = 'VERSION'
  s.summary          = 'TeamViewer Jitsi Meet iOS SDK'
  s.description      = 'TeamViewer Jitsi Meet is a WebRTC compatible, free and Open Source video conferencing system that provides browsers and mobile applications with Real Time Communications capabilities.'
  s.homepage         = 'https://gitlab.com/chatvisor/jitsi-meet-ios-sdk-releases'
  s.license          = 'Apache 2'
  s.authors          = 'The Jitsi Meet project authors', 'TeamViewer'
  s.source           = { :git => 'git@gitlab.com:chatvisor/jitsi-meet-ios-sdk-releases.git', :tag => s.version }

  s.platform         = :ios, '12.0'

  s.vendored_frameworks = 'Frameworks/JitsiMeetSDK.xcframework', 'Frameworks/WebRTC.xcframework'
  
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
